using System.ComponentModel.DataAnnotations;

namespace scrum_cards_backend.Models
{
    public class JoinGameModel
    {
        [Required]
        public string GameToken { get; set; }
    }
}