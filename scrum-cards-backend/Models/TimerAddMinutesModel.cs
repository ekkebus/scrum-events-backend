﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace scrum_cards_backend.Models
{
    public class TimerAddMinutesModel
    {
        [Range(1, 60)]
        [Required]
        public int Minutes { get; set; }
    }
}
