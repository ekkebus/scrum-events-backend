﻿using System.ComponentModel.DataAnnotations;

namespace scrum_cards_backend.Models
{
    public class AddNoteModel
    {
        [StringLength(100, MinimumLength = 0, ErrorMessage = "Note must be between 1 and 100 characters.")]
        public string Note { get; set; }
        [Required(ErrorMessage = "Card is empty.")]
        public int CardId { get; set; }
    }
}