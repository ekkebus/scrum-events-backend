namespace scrum_cards_backend.Models
{
    public class UserSettingsModel
    {
        public bool DarkMode { get; set; }
    }
}