﻿using System.ComponentModel.DataAnnotations;

namespace scrum_cards_backend.Models
{
    public class CreateGameModel
    {
        [StringLength(20,MinimumLength = 5,ErrorMessage = "Name should be between 5 and 20 characters long.")]
        [RegularExpression(@"^[a-zA-Z0-9.!&]+$", ErrorMessage = "Name can only contain normal characters!")]
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Game template is required.")]
        public int? GameTemplateId { get; set; }
        [Required]
        public bool DirectFeedback { get; set; }

    }
}