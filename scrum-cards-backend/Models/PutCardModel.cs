﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace scrum_cards_backend.Models
{
    public class PutCardModel
    {
        [Required]
        public int CardId { get; set; }
        [Required]
        public int ColumnId { get; set; }

        public int? FromColumnId { get; set; } = null;
    }
}