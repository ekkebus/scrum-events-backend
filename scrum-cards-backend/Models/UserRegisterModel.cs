﻿using System.ComponentModel.DataAnnotations;

namespace scrum_cards_backend.Models
{
    public class UserRegisterModel
    {
        [StringLength(50,MinimumLength = 2,ErrorMessage = "Username should be longer than 2 characters.")]
        [RegularExpression(@"^[a-zA-Z0-9_-]+$", ErrorMessage = "Username can only contain normal characters!")]
        [Required(ErrorMessage = "Username is required")]
        public string UserName { get; set; }
    }
}