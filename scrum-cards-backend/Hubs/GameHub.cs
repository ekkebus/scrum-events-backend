using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using scrum_cards_backend.DAL;
using scrum_cards_backend.Entities;
using scrum_cards_backend.Services;

namespace scrum_cards_backend.Hubs
{
    public class GameHub : Hub
    {
        public ScrumContext _context;
        
        public GameHub(ScrumContext context)
        {
            _context = context;
        }

        public override async Task OnConnectedAsync()
        {
            var user = await _context.Users.Include("Game.Players").Include("Game.CardStack").Include("Game.Columns.Cards")
                .FirstOrDefaultAsync(u => u.Id.ToString() == Context.UserIdentifier);
            if (user == null)
            {
                Dispose();
                throw new Exception("User doesn't exist.");
            }

            if (user.Game != null)
            {
                var game = user.Game;
                //var game = await _context.Games.Include(g => g.Players).FirstOrDefaultAsync(g => g.Id == user.Game.Id);
                //game.Players = game.Players.OrderBy(c => c.UserName).ToList();
                await Groups.RemoveFromGroupAsync(user.HubConnectionId, "game-" + game.JoinCode);
                await Groups.AddToGroupAsync(Context.ConnectionId, "game-" + game.JoinCode);
                
                user.HubConnectionId = Context.ConnectionId;
                await _context.SaveChangesAsync();
                
                List<Card>? correctCards = null;
                List<Card>? wrongCards = null;
                if (game.DirectFeedback || game.IsFinished)
                {
                    (correctCards, wrongCards) = game.GetCorrectAndWrongCards();
                }
                
                await Clients.GroupExcept("game-" + game.JoinCode, Context.ConnectionId).SendAsync("GameUpdate", JsonConvert.SerializeObject(new {game = game.ToSortedObject(!game.IsFinished), correctCards, wrongCards}, new JsonSerializerSettings
                {
                    ContractResolver = new DefaultContractResolver
                    {
                        NamingStrategy = new CamelCaseNamingStrategy()
                    }
                }));
                return;
            }

            user.HubConnectionId = Context.ConnectionId;
            await _context.SaveChangesAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var user = await _context.Users.Include("Game.Players")
                .FirstOrDefaultAsync(u => u.Id.ToString() == Context.UserIdentifier);
            if (user == null)
            {
                Dispose();
                throw new Exception("User doesn't exist.");
            }

            if (user.Game != null)
            {
                await Groups.RemoveFromGroupAsync(user.HubConnectionId, "game-" + user.Game.JoinCode);
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, "game-" + user.Game.JoinCode);
                if (user.Game.CurrentTurnPlayerId == user.Id) user.Game.NextPlayerTurn();
                var gameId = user.Game.Id;
                user.Game = null;
                await _context.SaveChangesAsync();
                
                var game = await _context.Games.Include("Players").Include("CardStack").Include("Columns.Cards").FirstOrDefaultAsync(g => g.Id == gameId);
                
                List<Card>? correctCards = null;
                List<Card>? wrongCards = null;
                if (game.DirectFeedback || game.IsFinished)
                {
                    (correctCards, wrongCards) = game.GetCorrectAndWrongCards();
                }
                
                await Clients.Group("game-" + game.JoinCode).SendAsync("GameUpdate", JsonConvert.SerializeObject(new {game = game.ToSortedObject(!game.IsFinished), wrongCards, correctCards}, new JsonSerializerSettings
                {
                    ContractResolver = new DefaultContractResolver
                    {
                        NamingStrategy = new CamelCaseNamingStrategy()
                    }
                }));
            }
        }
    }
}