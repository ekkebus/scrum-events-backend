﻿using System;
using System.Collections.Generic;
using scrum_cards_backend.Entities;

namespace scrum_cards_backend.Interfaces
{
    public interface IGameTemplate
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<ColumnTemplate> ColumnsTemplate { get; set; }
        public int TimerDuration { get; set; }
        public string CopyrightString { get; set; }
    }
}