﻿using System.Collections.Generic;
using scrum_cards_backend.Entities;

namespace scrum_cards_backend.Interfaces
{
    public interface IColumnTemplate
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public List<CardTemplate> Cards { get; set; }
    }
}