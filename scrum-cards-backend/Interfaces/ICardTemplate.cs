﻿using System;
using scrum_cards_backend.Entities;

namespace scrum_cards_backend.Interfaces
{
    public interface ICardTemplate
    {
        public int Id { get; set; }
        public ColumnTemplate Column { get; set; }
        public string Explanation { get; set; }
        public string Description { get; set; }
    }
}