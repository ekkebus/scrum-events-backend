﻿using System.Collections.Generic;
using scrum_cards_backend.Entities;

namespace scrum_cards_backend.Interfaces
{
    public interface IColumn
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int TemplateId { get; set; }
        public List<Card> Cards { get; set; }
    }
}