﻿using System;
using scrum_cards_backend.Entities;

namespace scrum_cards_backend.Interfaces
{
    public interface ICard
    {
        public int Id { get; set; }
        public Column Column { get; set; }
        public int? ColumnId { get; set; }
        public string Explanation { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public int CorrectColumnTemplateId { get; set; }
        public int? CardStackOrder { get; set; }

        public void AddNote(string note);

        public bool IsInCorrectColumn();
    }
}