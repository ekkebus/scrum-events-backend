﻿using System;
using System.Collections.Generic;
using scrum_cards_backend.Entities;

namespace scrum_cards_backend.Interfaces
{
    public interface IGame
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Column> Columns { get; set; }
        public List<Card> CardStack { get; set; }
        public GameTemplate GameTemplate { get; set; }
        public bool IsFinished { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime FinishTime { get; set; }
        public int TimerDuration { get; set; }
        public bool DirectFeedback { get; set; }
        public string CopyrightString { get; set; }
        public string JoinCode { get; set; }
        public int? CurrentTurnPlayerId { get; set; }

        public bool Skip();
        public bool CanFinish();
        public (List<Card> correctCards, List<Card> wrongCards) Finish();
        public Card GetCardById(int id);
        public (List<Card> correctCards, List<Card> wrongCards) ShowDirectFeedback();

        public Game ToSortedObject(bool hideExplanation = false);
        public bool TimerAddMinutes(int minutes);
    }
}