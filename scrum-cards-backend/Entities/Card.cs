﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using scrum_cards_backend.Interfaces;

namespace scrum_cards_backend.Entities
{
    public class Card : ICard
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Explanation { get; set; }
        [Required]
        public string Description { get; set; }
        public string? Note { get; set; }
        [Required]
        //Column id of the correct column the card belongs in
        public int CorrectColumnTemplateId { get; set; }
        //Used to order the card to stop desync between client and server
        public int? CardStackOrder { get; set; }
        //Column id if the column the card is placed in, is NULL if the card is still in the cardstack
        public int? ColumnId { get; set; }
        [JsonIgnore]
        public Column Column { get; set; }
        
        public void AddNote(string note)
        {
            Note = note;
        }

        public bool IsInCorrectColumn()
        {
            return Column.TemplateId == CorrectColumnTemplateId;
        }
    }
}