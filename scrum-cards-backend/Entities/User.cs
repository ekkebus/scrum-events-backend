﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace scrum_cards_backend.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        [JsonIgnore]
        public string UserSid { get; set; }
        [JsonIgnore]
        public string? HubConnectionId { get; set; }
        [JsonIgnore]
        public UserEvents UserEvents { get; set; }
        public bool DarkMode { get; set; }
        [JsonIgnore]
        public DateTime LastUse { get; set; } = DateTime.UtcNow;
        
        [ForeignKey("UserEvents")]
        [JsonIgnore]
        public int UserEventsId { get; set; }
        [JsonIgnore]
        public Game? Game { get; set; }
    }
}