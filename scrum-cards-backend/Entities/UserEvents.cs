﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace scrum_cards_backend.Entities
{
    public class UserEvents
    {
        [Key]
        public int Id { get; set; }
        public int PlacementCard { get; set; }
        public int SkipCard { get; set; }
        
    }
}