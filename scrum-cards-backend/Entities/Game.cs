﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using scrum_cards_backend.Interfaces;
using scrum_cards_backend.Utils;

namespace scrum_cards_backend.Entities
{
    public class Game : IGame
    {
        private List<Card> _cardStack = new List<Card>();

        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public List<Column> Columns { get; set; } = new List<Column>();
        [Required]
        [ForeignKey("CardStackGameId")]
        public List<Card> CardStack
        {
            get
            {
                return _cardStack = _cardStack.OrderBy(c => c.CardStackOrder).ToList();
            }
            set => _cardStack = value;
        }

        [Required]
        [JsonIgnore]
        public GameTemplate GameTemplate { get; set; }
        [Required]
        public bool IsFinished { get; set; }
        [Required]
        public bool IsTutorialFinished { get; set; }
        [Required]
        public bool DirectFeedback { get; set; }

        [Required]
        public DateTime StartTime { get; set; }
        [Required]
        public DateTime FinishTime { get; set; }
        [Required]
        public int TimerDuration { get; set; }
        
        public List<User> Players { get; set; }
        [Required]
        public string CopyrightString { get; set; }
        [Required]
        public string JoinCode { get; set; }

        public int? CurrentTurnPlayerId { get; set; }

        public Game()
        {
            IsFinished = false;
        }

        public Game(GameTemplate t, bool directFeedback) : this()
        {
            if (t == null) return;
            var cardList = new List<Card>();
            foreach (var columnTemplate in t.ColumnsTemplate)
            {
                var column = new Column
                {
                    Title = columnTemplate.Title,
                    TemplateId = columnTemplate.Id
                };
                Columns.Add(column);
                foreach (var columnTemplateCard in columnTemplate.Cards)
                {
                    cardList.Add(new Card
                    {
                        CorrectColumnTemplateId = columnTemplate.Id,
                        Description = columnTemplateCard.Description,
                        Explanation = columnTemplateCard.Explanation
                    });
                }
            }

            cardList.Shuffle();
            var order = 0;
            cardList.ForEach(c =>
            {
                c.CardStackOrder = order;
                order++;
            });

            DirectFeedback = directFeedback;
            TimerDuration = t.TimerDuration;
            CopyrightString = t.CopyrightString;
            CardStack = cardList;
            GameTemplate = t;
            IsTutorialFinished = false;
            StartTime = DateTime.UtcNow;
            JoinCode = ListUtils.GetRandomHexNumber(8).ToUpper();
        }

        /// <summary>
        /// Skips a card from the central pile by placing it last.
        /// </summary>
        /// <returns>if the action is performed via a bool.</returns>
        public bool Skip()
        {
            if (CardStack.Count < 2)
            {
                return false;
            }

            CardStack.First().CardStackOrder = CardStack.Last().CardStackOrder + 1;
            //CardStack.Add(CardStack.First());
            //CardStack.RemoveAt(0);
            return true;
        }

        /// <summary>
        /// Checks if the game can finish.
        /// </summary>
        /// <returns>returns the status of the bool finish game and if the carstack contains any elements.</returns>
        public bool CanFinish()
        {
            return !IsFinished && !CardStack.Any();
        }

        /// <summary>
        /// finishes the game by checking the status of the cards
        /// </summary>
        /// <returns>the correct and wrong cards</returns>
        public (List<Card> correctCards, List<Card> wrongCards) GetCorrectAndWrongCards()
        {
            var correctCards = new List<Card>();
            var wrongCards = new List<Card>();
            
            foreach (var card in Columns.SelectMany(column => column.Cards))
            {
                if (card.IsInCorrectColumn())
                {
                    correctCards.Add(card); // <-- Kaart is correct!
                }
                else
                {
                    wrongCards.Add(card); // <-- Kaart is incorrect!
                }
            }
            
            return (correctCards, wrongCards);
        }

        public (List<Card> correctCards, List<Card> wrongCards) Finish()
        {
            IsFinished = true;
            return GetCorrectAndWrongCards();
        }

        /// <summary>
        /// Allows one to select a card by it's card id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Card if a card is returnable. Else returns null.</returns>
        public Card GetCardById(int id)
        {
            var card = CardStack.FirstOrDefault(c => c.Id == id);
            if (card != null)
            {
                return card;
            }
            
            foreach (var column in Columns)
            {
                card = column.Cards.FirstOrDefault(c => c.Id == id);
                if (card != null) return card;
            }

            return null;
        }

        /// <summary>
        /// Same structure as finish game, does not change the IsFinished bool.
        /// </summary>
        /// <returns>the correct and wrong cards</returns>
        public (List<Card> correctCards, List<Card> wrongCards) ShowDirectFeedback()
        {
            var correctCards = new List<Card>();
            var wrongCards = new List<Card>();

            foreach (var card in Columns.SelectMany(column => column.Cards))
            {
                if (card.IsInCorrectColumn())
                {
                    correctCards.Add(card);
                }
                else
                {
                    wrongCards.Add(card);
                }
            }

            return (correctCards, wrongCards);
        }

        /// <summary>
        /// Removes explanation and the correctcolumntemplade id if so user cant check where it should go.
        /// </summary>
        /// <returns>Game without explanation and CorrectColumnTemplateId</returns>
        public Game ToSortedObject(bool hideExplanationAndColumn = false)
        {
            Columns = Columns.OrderBy(c => c.TemplateId).ToList();
            Players = Players.OrderBy(p => p.Id).ToList();
            Columns.ForEach(c =>
            {
                c.Cards.ForEach(x =>
                {
                    if (hideExplanationAndColumn && !DirectFeedback)
                    {
                        x.Explanation = "";
                        x.CorrectColumnTemplateId = 0;
                    }
                });
                c.Cards = c.Cards.OrderBy(a => a.CardStackOrder).ToList();
            });
            if (hideExplanationAndColumn) 
            {
                CardStack.ForEach(c =>
                {
                    c.Explanation = "";
                    c.CorrectColumnTemplateId = 0;
                });
            }
            return this;
        }


        /// <summary>
        /// Add minutes to the exsisting timer
        /// </summary>
        /// <param name="minutes"></param>
        /// <returns>true when the action is complete</returns>
        public bool TimerAddMinutes(int minutes)
        {
            var OldTimer = TimerDuration;
            var NewTimer = OldTimer + minutes;

            if (OldTimer == NewTimer)
            {
                return false;
            }

            TimerDuration = NewTimer;
            return true;
        }

        /// <summary>
        /// Setup the next player for their turn
        /// </summary>
        public void NextPlayerTurn()
        {
            var playerList = Players.OrderBy(p => p.Id).ToList();
            if (CurrentTurnPlayerId == null && playerList.Count > 0)
            {
                CurrentTurnPlayerId = playerList[0].Id;
            }

            try
            {
                var currentPlayerIndex = playerList.FindIndex(u => u.Id == CurrentTurnPlayerId);
                if ((playerList.Count - 1) <= currentPlayerIndex)
                {
                    CurrentTurnPlayerId = playerList[0].Id;
                }
                else
                {
                    CurrentTurnPlayerId = playerList[currentPlayerIndex + 1].Id;
                }
            }
            catch (ArgumentNullException e)
            {
                if (playerList.Count > 0)
                {
                    CurrentTurnPlayerId = playerList[0].Id;
                }
            }
        }
    }
}