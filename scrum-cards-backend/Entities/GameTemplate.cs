﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using scrum_cards_backend.Interfaces;

namespace scrum_cards_backend.Entities
{
    public class GameTemplate : IGameTemplate
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public List<ColumnTemplate> ColumnsTemplate { get; set; } = new List<ColumnTemplate>();
        [Required]
        public int TimerDuration { get; set; }
        [Required]
        public string CopyrightString { get; set; }
    }
}