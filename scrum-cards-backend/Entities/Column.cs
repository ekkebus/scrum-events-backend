﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using scrum_cards_backend.Interfaces;

namespace scrum_cards_backend.Entities
{
    public class Column : IColumn
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public int TemplateId { get; set; }
        public List<Card> Cards { get; set; } = new List<Card>();
    }
}