﻿using System.ComponentModel.DataAnnotations;
using scrum_cards_backend.Interfaces;

namespace scrum_cards_backend.Entities
{
    public class CardTemplate : ICardTemplate
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Explanation { get; set; }
        [Required]
        public string Description { get; set; }
        public ColumnTemplate Column { get; set; }
    }
}