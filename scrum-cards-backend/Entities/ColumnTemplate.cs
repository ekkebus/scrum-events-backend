﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using scrum_cards_backend.Interfaces;

namespace scrum_cards_backend.Entities
{
    public class ColumnTemplate : IColumnTemplate
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public List<CardTemplate> Cards { get; set; } = new List<CardTemplate>();
    }
}