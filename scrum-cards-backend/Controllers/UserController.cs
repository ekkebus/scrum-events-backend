using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using scrum_cards_backend.DAL;
using scrum_cards_backend.Entities;
using scrum_cards_backend.Hubs;
using scrum_cards_backend.Models;
using scrum_cards_backend.Services;
using scrum_cards_backend.Utils;

namespace scrum_cards_backend.Controllers
{
    [ApiController]
    [Route("api/user")]
    public class UserController : ControllerBase
    {
        private ScrumContext _context;
        private AppSettingsUtil _appSettingsUtil;
        private GameHubService? _hubService;
        
        public UserController(ScrumContext context, IOptions<AppSettingsUtil> appSettings, IHubContext<GameHub>? hub)
        {
            _context = context;
            _appSettingsUtil = appSettings.Value;
            if (hub != null) _hubService = new GameHubService(hub, context);
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterUser([FromBody] UserRegisterModel model)
        {
            var userEvents = new UserEvents
            {
                SkipCard = 0,
                PlacementCard = 0
            };
            await _context.UserEvents.AddAsync(userEvents);
            await _context.SaveChangesAsync();

            var user = new User
            {
                UserName = model.UserName,
                UserEventsId = userEvents.Id,
                UserSid = "",
                DarkMode = true
            };
            await _context.Users.AddAsync(user);

            await _context.SaveChangesAsync();
            
            var userSid = Guid.NewGuid().ToString();
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Environment.GetEnvironmentVariable("JWT_SECRET") ?? throw new InvalidOperationException());
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] 
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimTypes.Sid, userSid)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var stringToken = tokenHandler.WriteToken(token);

            user.UserSid = userSid;
            user.LastUse = DateTime.UtcNow;
            await _context.SaveChangesAsync();
            
            return Ok(new
            {
                Token = stringToken,
                user = new
                {
                    user.Id, user.UserName, user.DarkMode
                }
            });
        }

        [Authorize]
        [HttpGet("info")]
        public async Task<IActionResult> GetUserInfo()
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }
            
            user.LastUse = DateTime.UtcNow;
            await _context.SaveChangesAsync();

            return Ok(user);
        }

        [Authorize]
        [HttpGet("verify")]
        public async Task<IActionResult> VerifyUser()
        {
            var user = await _context.Users.Include("Game.CardStack").Include("Game.Columns.Cards").Include("Game.Players").FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }
            
            user.LastUse = DateTime.UtcNow;
            await _context.SaveChangesAsync();

            Game? g = null;
            List<Card>? correctCards = null;
            List<Card>? wrongCards = null;

            if (user.Game != null)
            {
                g = user.Game.ToSortedObject(!user.Game.IsFinished);

                if (g.IsFinished)
                {
                    (correctCards, wrongCards) = g.Finish();
                } else if (g.DirectFeedback)
                {
                    (correctCards, wrongCards) = g.GetCorrectAndWrongCards();
                }
            }

            return Ok(new
            {
                game = g,
                correctCards, wrongCards,
                user = new
                {
                    user.Id, user.UserName, user.DarkMode
                }
            });
        }
        
        [Authorize]
        [HttpPost("settings")]
        public async Task<IActionResult> SaveSettings([FromBody] UserSettingsModel model)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }

            user.DarkMode = model.DarkMode;
            user.LastUse = DateTime.UtcNow;

            await _context.SaveChangesAsync();
            return Ok(new
            {
                user.Id, user.UserName, user.DarkMode
            });
        }

        [Authorize]
        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            var user = await _context.Users.Include("Game")
                .FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }

            if (user.Game != null)
            {
                return BadRequest(new {message = "Please leave the current game first."});
            }
            
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return Ok(new {message = "Logged out."});
        }
    }
}