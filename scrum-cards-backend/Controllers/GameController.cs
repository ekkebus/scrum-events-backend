﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using scrum_cards_backend.DAL;
using scrum_cards_backend.Entities;
using scrum_cards_backend.Hubs;
using scrum_cards_backend.Models;
using scrum_cards_backend.Services;
using scrum_cards_backend.Utils;

namespace scrum_cards_backend.Controllers
{
    [ApiController]
    /*
    [Authorize]
    */
    [Route("api/game")]
    public class GameController : ControllerBase
    {
        private ScrumContext _context;
        private GameHubService? _hubService;
        
        public GameController(ScrumContext context, IHubContext<GameHub>? hub)
        {
            _context = context;
            if (hub != null) _hubService = new GameHubService(hub, context);
        }

        //URL/api/game/create
        //Creates new game based on the CreateGameModel
        [HttpPost("create")]
        public async Task<IActionResult> CreateGame([FromBody] CreateGameModel model)
        {
            //Check if user exists
            var user = await _context.Users.Include("Game")
                .FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }

            //Check if user is already in a game
            if (user.Game != null)
            {
                return BadRequest(new {message = "You're already in a game."});
            }
            
            //Create a game based on the CreateGameModel
            var gameTemplate = await _context.GameTemplates.Include(c => c.ColumnsTemplate).ThenInclude(x => x.Cards).FirstOrDefaultAsync(t => t.Id == model.GameTemplateId);
            
            //Check if gametemplate exists.
            if (gameTemplate == null)
            {
                return BadRequest(new {message = "Game template doesn't exist."});
            }
            
            //Actually create the new game
            var game = new Game(gameTemplate, model.DirectFeedback)
            {
                Name = model.Name,
                CurrentTurnPlayerId = user.Id
            };
            
            //Add game to database
            await _context.Games.AddAsync(game);
            //Set game to the user
            user.Game = game;
            user.LastUse = DateTime.UtcNow;

            if (user.HubConnectionId != null && _hubService != null) await _hubService.AddToGame(user.HubConnectionId, game.JoinCode);

            //Save changed to database
            await _context.SaveChangesAsync();
            
            //Return game object
            return Ok(game.ToSortedObject(true));
        }

        
        //URL/api/game/join
        //Joins game based on the JoinGameModel
        [HttpPost("join")]
        public async Task<IActionResult> JoinGame([FromBody] JoinGameModel model)
        {
            //Check if user exists
            var user = await _context.Users.Include("Game.Players").Include("Game.CardStack").Include("Game.Columns.Cards")
                .FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }
            
            //Check if user is already in a game
            if (user.Game != null)
            {
                return BadRequest(new {message = "You're already in a game."});
            }

            //Get game based on gametoken
            var game = await _context.Games.Include("Players").Include("CardStack").Include("Columns.Cards").FirstOrDefaultAsync(g => g.JoinCode == model.GameToken);

            //Check if game exists
            if (game == null)
            {
                return BadRequest(new {message = "This game doesn't exist."});
            }

            //Check if user is connected to hub
            if (user.HubConnectionId == null)
            {
                return BadRequest(new {message = "You're not connected to the live hub."});
            }

            //Connect game to user
            user.Game = game;
            user.LastUse = DateTime.UtcNow;
            
            //Adds user to game in hubservice
            if (_hubService != null) await _hubService.AddToGame(user.HubConnectionId, game.JoinCode);
            
            
            List<Card>? correctCards = null;
            List<Card>? wrongCards = null;
            
            //Return current correct and wrongcards based on gamestate
            if (game.IsFinished)
            {
                (correctCards, wrongCards) = game.Finish();
            } else if (game.DirectFeedback)
            {
                (correctCards, wrongCards) = game.GetCorrectAndWrongCards();
            }

            if (game.CurrentTurnPlayerId == null) game.CurrentTurnPlayerId = user.Id;

            await _context.SaveChangesAsync();
            
            if (_hubService != null && user.HubConnectionId != null) await _hubService.GameUpdate(game.ToSortedObject(!game.IsFinished), user.HubConnectionId);

            //returns game object with the correct and wrongcards
            return Ok(new
            {
                game = game.ToSortedObject(!game.IsFinished), correctCards, wrongCards
            });
        }

        //URL/api/game/finish
        //Try to finish a game, checks if all cards are placed.
        [HttpPost("finish")]
        public async Task<IActionResult> FinishGame()
        {
            //Check if user exists
            var user = await _context.Users.Include("Game.CardStack").Include("Game.Columns.Cards").Include("Game.Players")
                .FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }
            
            //Check if game exists
            if (user.Game == null)
            {
                return BadRequest(new {message = "Game doesn't exist."});
            }

            //Set game to user
            var game = user.Game;

            //Check if all cards are placed. 
            if (!game.CanFinish()) return BadRequest(new {message = "Not all cards have been placed yet!"});
            //Creates list filled with correct and wrongcards
            var (correctCards, wrongCards) = game.Finish();

            //Sets end time in database
            game.FinishTime = DateTime.UtcNow;
            user.LastUse = DateTime.UtcNow;

            //save changed to database
            await _context.SaveChangesAsync();
            
            if (_hubService != null && user.HubConnectionId != null) await _hubService.GameUpdate(game.ToSortedObject(), user.HubConnectionId);
            
            //Return game object + correct and wrongCards list.
            return Ok(new {game = game.ToSortedObject(), correctCards, wrongCards});
        }

        //URL/api/game/card/skip
        //Try to skip a card in the cardstack.
        [HttpPost("card/skip")]
        public async Task<IActionResult> SkipCard()
        {
            //Check if user exists
            var user = await _context.Users.Include("Game.CardStack").Include("Game.Columns.Cards").Include("Game.Players")
                .FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }

            //Check if game exists
            if (user.Game == null)
            {
                return BadRequest(new {message = "Game doesn't exist."});
            }
            
            //Set game to user
            var game = user.Game;

            //Check if its the current users turn. (Mostly important when playing multiplayer.
            if (game.CurrentTurnPlayerId != user.Id)
            {
                return BadRequest(new {message = "It's not your turn."});
            }

            //Check if you can still skip a card (Empty / one card left == cant skip)
            if (!game.Skip())
            {
                return BadRequest(new {message = "Card stack is (almost) empty."});
            }
            
            //Set event
            var getEvent =  (from u in _context.UserEvents where u.Id == user.Id select u).FirstOrDefaultAsync();
            var increment = getEvent.Result.SkipCard + 1;
            getEvent.Result.SkipCard = increment;

            //Save event in database
            _context.Update(getEvent.Result);
            
            //Check if tutorial is finished, to start the timer
            if (!game.IsTutorialFinished)
            {
                game.StartTime = DateTime.UtcNow;
                game.IsTutorialFinished = true;
            }
            
            //Next player gets the turn
            game.NextPlayerTurn();
            user.LastUse = DateTime.UtcNow;
            
            //save changed in database
            await _context.SaveChangesAsync();

            //Update game in hubservice
            if (user.HubConnectionId != null && _hubService != null)
                await _hubService.GameUpdate(game.ToSortedObject(true), user.HubConnectionId);
            
            //Return game object
            return Ok(game.ToSortedObject(true));
        }

        //URL/api/game/card/note/add
        //Add a note to a cartain card defined in the AddNoteModel. All data is taken from the AddNoteModel and each card can have their own note.
        [HttpPost("card/note/add")]
        public async Task<IActionResult> AddNote([FromBody] AddNoteModel model)
        {
            //Check if user exists
            var user = await _context.Users.Include("Game.CardStack").Include("Game.Columns.Cards").Include("Game.Players")
                .FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }

            //Check if game exists
            if (user.Game == null)
            {
                return BadRequest(new {message = "Game doesn't exist."});
            }

            //Set game to user
            var game = user.Game;

            //Get card based on the ID of card in AddNoteModel
            var card = game.GetCardById(model.CardId);
            //Check if card exists
            if (card == null)
            {
                return BadRequest(new { message = "Card does not exist."});
            }
            
            //Add note to card based on AddNoteModel
            card.AddNote(model.Note);
            user.LastUse = DateTime.UtcNow;

            //Save changes to database
            await _context.SaveChangesAsync();
            
            
            if (_hubService != null && user.HubConnectionId != null) await _hubService.GameUpdate(game.ToSortedObject(!game.IsFinished), user.HubConnectionId);

            //Return game object. If game IsFinished then ToSortedObject(false)
            return Ok(game.ToSortedObject(!game.IsFinished));
        }

        //URL/api/game/card/put
        //Put a card from the cardstack into a column or from a column into a different column
        [HttpPost("card/put")]
        public async Task<IActionResult> PutCard([FromBody] PutCardModel model)
        {
            //Check if user exists
            var user = await _context.Users.Include("Game.CardStack").Include("Game.Columns.Cards").Include("Game.Players")
                .FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }

            //Check if game exists
            if (user.Game == null)
            {
                return BadRequest(new {message = "Game doesn't exist."});
            }

            //Set game to user
            var game = user.Game;

            //Check if column exists.
            var column = game.Columns.FirstOrDefault(c => c.Id == model.ColumnId);
            if (column == null)
            {
                return BadRequest(new {message = "Column 'to' doesn't exist."});
            }
            
            Card card;
            
            //Check if card comes from cardstack or column
            //Top part is run when its from the cardstack and moved the card from the cardstack to said column
            if (model.FromColumnId == null)
            {
                if (game.CurrentTurnPlayerId != user.Id)
                {
                    return BadRequest(new {message = "It's not your turn."});
                }
                if (game.CardStack.FirstOrDefault() != null)
                {
                    if (game.CardStack.First().Id == model.CardId)
                    {
                        card = game.CardStack.First();
                        game.CardStack.RemoveAt(0);
                        game.NextPlayerTurn();
                    }
                    else
                    {
                        return BadRequest(new {message = "Card is not first in cardstack."});
                    }
                }
                else
                {
                    return BadRequest(new {message = "Card stack is empty."});
                }
            }
            //This part is run when the card comes from a column and is placed inside a different column
            else
            {
                if (game.CurrentTurnPlayerId != user.Id)
                {
                    return BadRequest(new {message = "Please wait your turn."});
                }
                var fromColumn = game.Columns.FirstOrDefault(c => c.Id == model.FromColumnId);
                if (fromColumn == null)
                {
                    return BadRequest(new {message = "Column 'from' doesn't exist."});
                }
                card = fromColumn.Cards.FirstOrDefault(c => c.Id == model.CardId);
                    
                if (card == null)
                {
                    return BadRequest(new {message = "Card doesn't exist."});
                }

                fromColumn.Cards.Remove(card);
            }
            
            //Orders the carstack properly again
            var lastCardInColumn = column.Cards.OrderBy(c => c.CardStackOrder).LastOrDefault();
            if (lastCardInColumn != null)
            {
                card.CardStackOrder = lastCardInColumn.CardStackOrder + 1;
            }
            else
            {
                card.CardStackOrder = 0;
            }

            //Add card to column
            column.Cards.Add(card);

            
            //Set event
            var getEvent =  (from u in _context.UserEvents where u.Id == user.Id select u).FirstOrDefaultAsync();
            var increment = getEvent.Result.PlacementCard + 1;
            getEvent.Result.PlacementCard = increment;
            
            //Save event in database
            _context.Update(getEvent.Result);
            user.LastUse = DateTime.UtcNow;
            await _context.SaveChangesAsync();
            
            if (game.DirectFeedback)
            {
                var (correctCards, wrongCards) = game.ShowDirectFeedback();
                if (_hubService != null && user.HubConnectionId != null) await _hubService.GameUpdate(game.ToSortedObject(true), user.HubConnectionId);
                return Ok(new { game = game.ToSortedObject(true), correctCards, wrongCards });
            }
            
            if (!game.IsTutorialFinished)
            {
                game.StartTime = DateTime.UtcNow;
                game.IsTutorialFinished = true;
                await _context.SaveChangesAsync();
            }
            
            if (_hubService != null && user.HubConnectionId != null) await _hubService.GameUpdate(game.ToSortedObject(true), user.HubConnectionId);
            
            //Return game object
            return Ok(game.ToSortedObject(true));
        }

        //URL/api/game/timer/add-minutes
        //Add a certain amount of time defined in the TimerAddMinutesModel
        [HttpPost("timer/add-minutes")]
        public async Task<IActionResult> TimerAddMinutes([FromBody] TimerAddMinutesModel model)
        {
            //Check if user exists
            var user = await _context.Users.Include("Game.CardStack").Include("Game.Columns.Cards").Include("Game.Players")
                .FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }
            
            //Check if game exists
            if (user.Game == null)
            {
                return BadRequest(new {message = "Game doesn't exist."});
            }

            //Set game to user
            var game = user.Game;

            //Check if game is already finished or not
            if (game.IsFinished)
            {
                return BadRequest(new {message = "The game has already finished."});
            }
            
            //Check if given time is above 0
            if (model.Minutes <= 0)
            {
                return BadRequest(new { message = "You can't add minutes lower or equal to zero" });
            }

            if (model.Minutes + game.TimerDuration > 60)
            {
                return BadRequest(new {message = "You can't add more minutes."});
            }

            if (!game.TimerAddMinutes(model.Minutes))
            {
                return BadRequest(new { message = "TimerAddMinutes Could not be performed" });
            }
            
            user.LastUse = DateTime.UtcNow;
            
            //Save changes in database
            await _context.SaveChangesAsync();
            
            if (_hubService != null && user.HubConnectionId != null) await _hubService.GameUpdate(game.ToSortedObject(true), user.HubConnectionId);
            
            //Return new object containing the new timer
            return Ok(new {timerDuration = game.TimerDuration});   
        }

        //URL/api/game/finish
        //Finishes a game, only possible if all cards are placed.
        [HttpPost("tutorial/finish")]
        public async Task<IActionResult> FinishTutorial()
        {
            //Check if user exists
            var user = await _context.Users.Include("Game.CardStack").Include("Game.Columns.Cards").Include("Game.Players")
                .FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }

            //Check if game exists
            if (user.Game == null)
            {
                return BadRequest(new {message = "Game doesn't exist."});
            }

            //Set game to user
            var game = user.Game;

            if (game.IsFinished)
            {
                return BadRequest(new { message = "The game is already finished." });
            }

            //Check if game is already finished or not
            if (!game.IsTutorialFinished)
            {
                game.StartTime = DateTime.UtcNow;
            }
            
            //Sets tutorialFinished to true on game.
            game.IsTutorialFinished = true;
            user.LastUse = DateTime.UtcNow;

            //Save changed to database
            await _context.SaveChangesAsync();
            
            if (_hubService != null && user.HubConnectionId != null) await _hubService.GameUpdate(game.ToSortedObject(true), user.HubConnectionId);
            
            //Return game object
            return Ok(game.ToSortedObject(true));
        }

        //URL/api/game/timer/time-passed
        //Gives the amount of that time has passed in the current game
        [HttpGet("timer/time-passed")]
        public async Task<IActionResult> ShowTimePassed()
        {
            //Check if user exists
            var user = await _context.Users.Include("Game.CardStack").Include("Game.Columns.Cards").Include("Game.Players")
                .FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }

            //Check if game exists
            if (user.Game == null)
            {
                return BadRequest(new {message = "Game doesn't exist."});
            }

            //Set game to user
            var game = user.Game;

            //Check if game is already finished or not
            if (!game.IsFinished)
            {
                return BadRequest(new { message = "Time passed can only be viewed when the game is finished. " });
            }
            
            if (game.FinishTime <= game.StartTime)
            {
                return BadRequest(new { message = "No time has passed. "});
            }

            //Return game object
            return Ok(game.FinishTime.Subtract(game.StartTime));
        }

        //URL/api/game/reset
        //Places all cards from the columns back into the cardstack
        [HttpPost("reset")]
        public async Task<IActionResult> Reset()
        {
            //Check if user exists
            var user = await _context.Users.Include("Game.CardStack").Include("Game.Columns.Cards").Include("Game.Players")
                .FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (user == null)
            {
                return BadRequest(new {message = "User doesn't exist."});
            }
            
            //Check if game exists
            if (user.Game == null)
            {
                return BadRequest(new {message = "Game doesn't exist."});
            }

            //Set game to user
            var game = user.Game;

            if (game.IsFinished) return BadRequest(new {message = "Game is already finished!"});
            if (game.DirectFeedback) return BadRequest(new {message = "Game has direct feedback enabled."});
            
            //Loop trough all cards in all columns.
            //Remove all cards from said columns and add the cards back to the cardstack.
            foreach (var column in game.Columns.ToList())
            {
                foreach (var card in column.Cards.ToList())
                {
                    game.CardStack.Add(card);
                    column.Cards.Remove(card);
                }
            }

            //Order the whole cardstack again to work against desync from client and server.
            var cardList = new List<Card>();

            cardList = game.CardStack;
            
            cardList.Shuffle();
            var order = 0;
            cardList.ForEach(c =>
            {
                c.CardStackOrder = order;
                order++;
            });

            //Set the new cardstack for the game
            game.CardStack = cardList;
            user.LastUse = DateTime.UtcNow;
            
            //Save the game in the database
            await _context.SaveChangesAsync();

            if (_hubService != null && user.HubConnectionId != null) await _hubService.GameUpdate(game.ToSortedObject(true), user.HubConnectionId);

            //Return game object with the new cardstack
            return Ok(game.ToSortedObject(true));
        }

        //URL/api/game/games/list
        //Returns a list of all games possible to play from the database
        [HttpGet("games/list")]
        public async Task<IActionResult> GetGames()
        {
            var gameList =  new List<GameTemplate>();
            foreach (var g in _context.GameTemplates)
            {
                gameList.Add(g);
            }
            return Ok(gameList);
        }

        //URL/api/game/leave
        //Leave the games ur user is connected to
        [HttpPost("leave")]
        public async Task<IActionResult> LeaveGame()
        {
            //Check if user exists
            var user = await _context.Users.Include("Game.Players").FirstOrDefaultAsync(u => u.Id == int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));

            //Check if user is in a game
            if (user.Game == null)
            {
                return BadRequest(new {message = "You're not in a game."});
            }
            
            if (user.HubConnectionId != null && _hubService != null) await _hubService.RemoveFromGame(user.HubConnectionId, user.Game.JoinCode);
            //if (user.Game.CurrentTurnPlayerId == user.Id) user.Game.CurrentTurnPlayerId = null;
            if (user.Game.CurrentTurnPlayerId == user.Id) user.Game.NextPlayerTurn();
            var gameId = user.Game.Id;

            user.Game = null;
            user.LastUse = DateTime.UtcNow;
            await _context.SaveChangesAsync();

            if (_hubService != null)
            {
                var game = await _context.Games.Include("Players").Include("CardStack").Include("Columns.Cards").FirstOrDefaultAsync(g => g.Id == gameId);
                await _hubService.GameUpdate(game.ToSortedObject(!game.IsFinished), "");
            }

            return Ok();
        }
        
        //URL/api/game/events/all
        //Returns all events
        [HttpGet("events/all")]
        public async Task<IActionResult> GetEvents()
        {
            var events = await _context.UserEvents.ToListAsync();
            return Ok(events);
        }

        [HttpGet("events/create-time")]
        public async Task<IActionResult> GetCreateTimer()
        {
            var time = await _context.Games.Select(s => s.StartTime).FirstOrDefaultAsync();
            if (time == null)
            {
                return BadRequest(new {message = "No timer found"});
            }
            return Ok(time);
        }
        
        [HttpGet("events/total-games")]
        public async Task<IActionResult> TotalGames()
        {
            var games = await _context.Games.CountAsync();
            return Ok(games);
        }
        
    }
}