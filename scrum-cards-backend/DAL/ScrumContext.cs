﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using scrum_cards_backend.Entities;

namespace scrum_cards_backend.DAL
{
    public class ScrumContext : DbContext
    {
        public DbSet<Game> Games { get; set; }
        public DbSet<Column> Columns { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<GameTemplate> GameTemplates { get; set; }
        public DbSet<ColumnTemplate> ColumnTemplates { get; set; }
        public DbSet<CardTemplate> CardTemplates { get; set; }
        public DbSet<User> Users { get; set; }
        
        public DbSet<UserEvents> UserEvents { get; set; }

        public ScrumContext(DbContextOptions<ScrumContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Column>().HasMany(c => c.Cards).WithOne(c => c.Column);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }
    }
}