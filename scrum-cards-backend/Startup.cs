using System;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using scrum_cards_backend.DAL;
using scrum_cards_backend.Hubs;
using scrum_cards_backend.Services;
using scrum_cards_backend.Utils;

namespace scrum_cards_backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
#if DEBUG
            services.AddDbContext<ScrumContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("ScrumContextLocal")));
#else
            if (Environment.GetEnvironmentVariable("DATABASE_URL") != null)
            {
                var dUrl = Environment.GetEnvironmentVariable("DATABASE_URL");
                var builder = new PostgreSqlConnectionStringBuilder(dUrl)
                {
                    Pooling = true,
                    TrustServerCertificate = true,
                    SslMode = SslMode.Require
                };

                services.AddDbContext<ScrumContext>(options => options.UseNpgsql(builder.ConnectionString));
            }
            else
            {
                services.AddDbContext<ScrumContext>(options =>
                    options.UseNpgsql(Configuration.GetConnectionString("ScrumContextLocal")));
            }      
#endif

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettingsUtil>(appSettingsSection);
            
            services.AddSignalR();
            
            /*
             * Bearer token auth
             */

            var appSettings = appSettingsSection.Get<AppSettingsUtil>();
            var key = Encoding.ASCII.GetBytes(Environment.GetEnvironmentVariable("JWT_SECRET") ?? throw new InvalidOperationException());
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };
                x.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        var accessToken = context.Request.Query["access_token"];

                        var path = context.HttpContext.Request.Path;
                        if (!string.IsNullOrEmpty(accessToken) &&
                            (path.StartsWithSegments("/api/game/live-hub")))
                        {
                            context.Token = accessToken;
                        }
                        return Task.CompletedTask;
                    },
                    OnTokenValidated = async context =>
                    {
                        if (!context.Principal.HasClaim(c => c.Type == ClaimTypes.Sid))
                        {
                            context.Fail("No SID found.");
                        }

                        var user = context.Principal;
                        var db = context.HttpContext.RequestServices.GetRequiredService<ScrumContext>();

                        var userId = user.FindFirstValue(ClaimTypes.NameIdentifier);
                        var userSid = user.FindFirstValue(ClaimTypes.Sid);

                        var hasSidStored = await db.Users.AnyAsync(c => c.Id.ToString() == userId && c.UserSid == userSid);
                        if (!hasSidStored)
                        {
                            context.Fail("Invalid SID.");
                        }
                    }
                };
            });

            services.AddAuthorization();
            services.AddCors();
            
            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);

            services.AddHostedService<HerokuPatchWorkerService>();
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ScrumContext context)
        {
            //context.Database.Migrate();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseStatusCodePages(async codeContext =>
            {
                codeContext.HttpContext.Response.ContentType = "application/json";
                await codeContext.HttpContext.Response.WriteAsync(JsonConvert.SerializeObject(new {message = ReasonPhrases.GetReasonPhrase(codeContext.HttpContext.Response.StatusCode), code = codeContext.HttpContext.Response.StatusCode}), Encoding.UTF8);

            });
            
            app.UseCors(x => x
                .WithOrigins("http://localhost:8080", "https://*.gitlab.io").SetIsOriginAllowedToAllowWildcardSubdomains()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<GameHub>("/api/game/live-hub");
            });
        }
    }
}