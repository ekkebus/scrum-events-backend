using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using scrum_cards_backend.DAL;

namespace scrum_cards_backend.Services
{
    public class HerokuPatchWorkerService : IHostedService, IDisposable
    {
        private Timer? _timer;
        private IServiceScopeFactory _scopeFactory;

        private int _counter;

        public HerokuPatchWorkerService(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ScrumContext>();
                context.Database.Migrate();
            }

            _timer = new Timer(ExecuteTask, null, TimeSpan.Zero, TimeSpan.FromHours(6));
            return Task.CompletedTask;
        }

        private void ExecuteTask(object? state)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ScrumContext>();
                var compareDate1 = DateTime.UtcNow.Subtract(new TimeSpan(0, 6, 0, 0, 0));
                var games = context.Games.Include("CardStack").Include("Columns.Cards").Include("Players")
                    .Where(g => g.StartTime <= compareDate1).ToList();
                foreach (var game in games)
                {
                    context.Cards.RemoveRange(game.CardStack);
                    foreach (var column in game.Columns)
                    {
                        context.Cards.RemoveRange(column.Cards);
                        context.Columns.Remove(column);
                    }

                    game.Players.Clear();
                }

                context.Games.RemoveRange(games);

                var compareDate2 = DateTime.UtcNow.Subtract(new TimeSpan(1, 0, 0, 0, 0));
                var users = context.Users.Where(u => u.LastUse <= compareDate2).ToList();
                foreach (var user in users)
                {
                    var uEvent = context.UserEvents.FirstOrDefault(x => x.Id == user.UserEventsId);
                    if (uEvent != null)
                    {
                        context.UserEvents.Remove(uEvent);
                    }
                }
                context.Users.RemoveRange(users);

                context.SaveChanges();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}