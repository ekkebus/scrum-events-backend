using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using scrum_cards_backend.DAL;
using scrum_cards_backend.Entities;
using scrum_cards_backend.Hubs;

namespace scrum_cards_backend.Services
{
    public class GameHubService
    {
        private IHubContext<GameHub> _hub;
        private ScrumContext _dbContext;

        public GameHubService(IHubContext<GameHub> context, ScrumContext dbContext)
        {
            _hub = context;
            _dbContext = dbContext;
        }
        
        public async Task AddToGame(string connectionId, string token)
        {
            await _hub.Groups.AddToGroupAsync(connectionId, "game-" + token);
        }
        
        public async Task RemoveFromGame(string connectionId, string token)
        {
            await _hub.Groups.RemoveFromGroupAsync(connectionId, "game-" + token);
        }

        public async Task TestMethod(string token, string exclude = "")
        {
            await SendMethod(token, "TestMethod", new {message = "Yay it works boi"}, exclude);
        }

        public async Task GameUpdate(Game game, string senderHubId)
        {
            List<Card>? correctCards = null;
            List<Card>? wrongCards = null;
            if (game.DirectFeedback || game.IsFinished)
            {
                (correctCards, wrongCards) = game.GetCorrectAndWrongCards();
            }

            await SendMethod(game.JoinCode, "GameUpdate", new {game = game.ToSortedObject(!game.IsFinished), correctCards, wrongCards}, senderHubId);
        }

        private async Task SendMethod(string token, string method, object data, string excludeConnection = "")
        {
            await _hub.Clients.GroupExcept("game-" + token, excludeConnection).SendAsync(method, JsonConvert.SerializeObject(data, new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                }
            }));
        }
    }
}