using Microsoft.EntityFrameworkCore;
using scrum_cards_backend.DAL;

namespace scrum_cards_backend_tests.DAL
{
    public class ScrumTestContextFactory
    {
        public ScrumContext GetScrumTestContext()
        {
            var options = new DbContextOptionsBuilder<ScrumContext>()
                .UseSqlite("Data Source=scrumtests.db")
                .Options;
            return new ScrumContext(options);
        }
    }
}