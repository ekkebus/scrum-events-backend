using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using scrum_cards_backend.Controllers;
using scrum_cards_backend.DAL;
using scrum_cards_backend.Entities;
using scrum_cards_backend.Models;
using scrum_cards_backend.Utils;
using scrum_cards_backend_tests.DAL;

namespace scrum_cards_backend_tests
{
    [TestFixture]
    public class GameControllerTests
    {
        private readonly ScrumContext _dbContext;
        private readonly GameController _controller;
        private readonly UserController _userController;

        public GameControllerTests()
        {
            _dbContext = new ScrumTestContextFactory().GetScrumTestContext();
            _dbContext.Database.EnsureDeleted();
            _dbContext.Database.Migrate();

            var options = Options.Create(new AppSettingsUtil());

            var user = new UserRegisterModel
            {
                UserName = "TestuserGameController"
            };

            _userController = new UserController(_dbContext, options, null);

            _userController.RegisterUser(user);

            var userObject = _dbContext.Users.First(u => u.UserName == "TestuserGameController");
            
            var mockUser = new ClaimsPrincipal(new ClaimsIdentity(new []
            {
                new Claim(ClaimTypes.NameIdentifier, userObject.Id.ToString()),
                new Claim(ClaimTypes.Sid, userObject.UserSid)
            }, "mock"));

            _controller = new GameController(_dbContext, null)
            {
                ControllerContext = new ControllerContext() {HttpContext = new DefaultHttpContext() {User = mockUser}}
            };

            CreateTemplates();
        }

        [Test]
        public async Task CreateGame_ReturnsOk()
        {
            await _controller.LeaveGame();
            var m = new CreateGameModel
            {
                Name = "TestGame",
                GameTemplateId = 1,
                DirectFeedback = false
            };
            Assert.IsTrue(Validator.TryValidateObject(m, new ValidationContext(m), new List<ValidationResult>(), true));
            var result = await _controller.CreateGame(m);
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public async Task CreateGame_ReturnsBadRequest()
        {
            await _controller.LeaveGame();
            var m = new CreateGameModel
            {
                Name = "hardheadedness is dumb"
            };

            Assert.IsFalse(Validator.TryValidateObject(m, new ValidationContext(m), new List<ValidationResult>(),
                true));
        }

        /*[Test]
        public async Task AddNote_ToDifferentGame_ReturnsBadRequest()
        {
            var g1 = (await _controller.CreateGame(new CreateGameModel // Game 1
            {
                Name = "Game1",
                GameTemplateId = 1
            }) as OkObjectResult)?.Value as Game;
                        
            var g2 = (await _controller.CreateGame(new CreateGameModel // Game 2
            {
                Name = "Game2",
                GameTemplateId = 1
            }) as OkObjectResult)?.Value as Game;
            
            Assert.IsNotNull(g2);

            var cardFrom2 = g2.CardStack.First();

            Assert.IsNotNull(g1);

            var result = await _controller.AddNote(new AddNoteModel
            {
                Note = "Failed note.",
                CardId = cardFrom2.Id
            });
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }*/

        [Test]
        public async Task AddNote_ReturnsOk()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "Game1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var card1 = g1.CardStack.First();

            var result = await _controller.AddNote(new AddNoteModel
            {
                CardId = card1.Id,
                Note = "This is a working note."
            });

            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public async Task PutCard_PlayNotFirstCardFromStack_ReturnsBadRequest()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "Game1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var wrongCard = g1.CardStack[^1];
            var columnTo = g1.Columns.First();

            var result = await _controller.PutCard(new PutCardModel
            {
                CardId = wrongCard.Id,
                ColumnId = columnTo.Id
            });

            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }

        /*[Test]
        public async Task PutCard_WrongGame_ReturnsBadRequest()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel // Game 1
            {
                Name = "Game1",
                GameTemplateId = 1
            }) as OkObjectResult)?.Value as Game;

            var g2 = (await _controller.CreateGame(new CreateGameModel // Game 2
            {
                Name = "Game2",
                GameTemplateId = 1
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var cardFrom2 = g2.CardStack.First();

            var result = await _controller.PutCard(g1.Id, new PutCardModel
            {
                CardId = cardFrom2.Id,
                ColumnId = g1.Columns.First().Id
            });
            
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }*/

        [Test]
        public async Task PutCard_ReturnsOk()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "Game1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var card = g1.CardStack.First();
            var columnTo = g1.Columns.First();

            var result1 = await _controller.PutCard(new PutCardModel
            {
                CardId = card.Id,
                ColumnId = columnTo.Id
            });

            Assert.IsInstanceOf<OkObjectResult>(result1);

            var result2 = await _controller.PutCard(new PutCardModel
            {
                CardId = columnTo.Cards.First().Id,
                ColumnId = g1.Columns.Last().Id,
                FromColumnId = columnTo.Id
            });

            Assert.IsInstanceOf<OkObjectResult>(result2);
            Assert.AreEqual(8, g1.Players.First().UserEvents.PlacementCard);

        }

        [Test]
        public async Task FinishGame_ReturnsOk()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var gameId = g1.Id;

            var card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});

            var result = await _controller.FinishGame();
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public async Task FinishGame_ReturnsBadRequest()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var gameId = g1.Id;

            var result = await _controller.FinishGame();
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }

        #region Unused Explanation Tests

        /*[Test]
        public async Task ShowExplanationNonExistingCard_ReturnsBadRequest()
        {
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1
            }) as OkObjectResult)?.Value as Game;

            Debug.Assert(g1 != null, nameof(g1) + " != null");
            var gameId = g1.Id;

            var card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});

            await _controller.FinishGame();

            var result = await _controller.ShowExplanation(5);
            
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }
        
        [Test]
        public async Task ShowExplanationNonExistingGame_ReturnsBadRequest()
        {
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1
            }) as OkObjectResult)?.Value as Game;

            Debug.Assert(g1 != null, nameof(g1) + " != null");
            var gameId = g1.Id;

            var card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2}); 
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2}); 
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2}); 
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2}); 
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2}); 
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2}); 

            await _controller.FinishGame();

            var result = await _controller.ShowExplanation(2000, 1);
            
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }
        
        [Test]
        public async Task ShowExplanationNonFinishedGame_ReturnsBadRequest()
        {
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1
            }) as OkObjectResult)?.Value as Game;

            Debug.Assert(g1 != null, nameof(g1) + " != null");
            var gameId = g1.Id;

            var card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});

            var result = await _controller.ShowExplanation(1);
            
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }
        
        [Test]
        public async Task ShowExplanation_ReturnsOk()
        {
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1
            }) as OkObjectResult)?.Value as Game;

            Debug.Assert(g1 != null, nameof(g1) + " != null");
            var gameId = g1.Id;

            var card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(gameId, new PutCardModel {CardId=card.Id, ColumnId = gameId*2});

            await _controller.FinishGame();

            var result = await _controller.ShowExplanation(card.Id);
            
            Assert.IsInstanceOf<OkObjectResult>(result);
        }*/
        
        
        
    
        

        #endregion

        [Test]
        public async Task SkipCard_ReturnsOk()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var result = await _controller.SkipCard();

            Assert.IsInstanceOf<OkObjectResult>(result);
            
            Assert.AreEqual(g1.Players.First().UserEvents.SkipCard, 1);

        }

        [Test]
        public async Task SkipCardMultiple_ReturnsOk()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            await _controller.SkipCard();

            var card1 = g1.CardStack.First();

            await _controller.SkipCard();

            var card2 = g1.CardStack.First();

            await _controller.SkipCard();

            var card3 = g1.CardStack.First();
            Assert.AreNotEqual(card3.Id, card2.Id);
            Assert.AreEqual(4, g1.Players.First().UserEvents.SkipCard);

        }


        [Test]
        public async Task SkipCardOneCardLeft_BadRequest()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var gameId = g1.Id;

            var card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            
            
            var result = await _controller.SkipCard();

            Assert.IsInstanceOf<BadRequestObjectResult>(result);
            Assert.AreEqual(29, g1.Players.First().UserEvents.PlacementCard);

        }

        [Test]
        public async Task SkipCardEmptyStack_BadRequest()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var gameId = g1.Id;

            var card = g1.CardStack.First();
            
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            
            var result = await _controller.SkipCard();

            Assert.IsInstanceOf<BadRequestObjectResult>(result);
            Assert.AreEqual(24, g1.Players.First().UserEvents.PlacementCard);

        }

        [Test]
        public async Task AddNegativeMinutes_BadRequest()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var result = await _controller.TimerAddMinutes(new TimerAddMinutesModel {Minutes = -1});

            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }

        [Test]
        public async Task AddMinutes_Ok_TimerDuration()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var result = await _controller.TimerAddMinutes(new TimerAddMinutesModel {Minutes = 10});
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public async Task ShowTimePassed_FinishTimeEqualsStartTime_BadRequestObject()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;
            Assert.IsNotNull(g1);
            g1.FinishTime = g1.StartTime;
            var result = await _controller.ShowTimePassed();

            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }

        [Test]
        public async Task ResetGame_OkRequest()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var gameId = g1.Id;

            var result = await _controller.Reset();

            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public async Task ResetGameAlreadyFinished_BadRequest()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);

            var gameId = g1.Id;

            var card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});

            await _controller.FinishGame();

            var result = await _controller.Reset();

            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }

        [Test]
        public async Task ResetGameAfterPuttingCards_BadRequest()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            Assert.IsNotNull(g1);
            var gameId = g1.Id;

            var card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});
            card = g1.CardStack.First();
            await _controller.PutCard(new PutCardModel {CardId=card.Id, ColumnId = gameId*2});

            await _controller.Reset();

            Assert.AreEqual(g1.CardStack.Count, 6);
        }


        [Test]
        public async Task ShowTimePassed_Ok()
        {
            await _controller.LeaveGame();
            var g1 = (await _controller.CreateGame(new CreateGameModel
            {
                Name = "GameName1",
                GameTemplateId = 1,
                DirectFeedback = false
            }) as OkObjectResult)?.Value as Game;

            g1.IsFinished = true;

            g1.FinishTime = DateTime.Now;
            
            var result = await _controller.ShowTimePassed();

            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        private void CreateTemplates()
        {
            var columnTemplate1 = new ColumnTemplate
            {
                Title = "Column 1",
                Cards = new List<CardTemplate>
                {
                    new CardTemplate
                    {
                        Description = "A first testcard for c1",
                        Explanation = "This is an awesome explanation"
                    },
                    new CardTemplate
                    {
                        Description = "A second testcard for c1",
                        Explanation = "This is an awesome explanation"
                    },
                    new CardTemplate
                    {
                        Description = "A third testcard for c1",
                        Explanation = "This is an awesome explanation"
                    }
                }
            };
            
            var columnTemplate2 = new ColumnTemplate
            {
                Title = "Column 2",
                Cards = new List<CardTemplate>
                {
                    new CardTemplate
                    {
                        Description = "A first testcard for c2",
                        Explanation = "This is an awesome explanation"
                    },
                    new CardTemplate
                    {
                        Description = "A second testcard for c2",
                        Explanation = "This is an awesome explanation"
                    },
                    new CardTemplate
                    {
                        Description = "A third testcard for c2",
                        Explanation = "This is an awesome explanation"
                    }
                }
            };

            _dbContext.GameTemplates.Add(new GameTemplate
            {
                Description = "Test game template to use in unit tests.",
                Title = "TestTemplate",
                TimerDuration = 10,
                CopyrightString = "COPYRIGHTTT",
                ColumnsTemplate = new List<ColumnTemplate>
                {
                    columnTemplate1, columnTemplate2
                }

            }) ;

            _dbContext.SaveChanges();
        }
    }
}