﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using scrum_cards_backend.Controllers;
using scrum_cards_backend.DAL;
using scrum_cards_backend.Models;
using scrum_cards_backend.Utils;
using scrum_cards_backend_tests.DAL;

namespace scrum_cards_backend_tests
{
    [TestFixture]
    public class UserControllerTests
    {
        private readonly ScrumContext _dbContext;
        private readonly UserController _userController;
        private UserRegisterModel user;
        
        
        public UserControllerTests()
        {
            _dbContext = new ScrumTestContextFactory().GetScrumTestContext();
            _dbContext.Database.EnsureDeleted();
            _dbContext.Database.Migrate();

            var options = Options.Create(new AppSettingsUtil());
            
            user = new UserRegisterModel
            {
                UserName = "TestUserController"
            };

            _userController = new UserController(_dbContext, options, null);

            _userController.RegisterUser(user);
            
            var userObject = _dbContext.Users.First(u => u.UserName == "TestUserController");
            
            var mockUser = new ClaimsPrincipal(new ClaimsIdentity(new []
            {
                new Claim(ClaimTypes.NameIdentifier, userObject.Id.ToString()),
                new Claim(ClaimTypes.Sid, userObject.UserSid)
            }, "mock"));

            _userController = new UserController(_dbContext, options, null)
            {
                ControllerContext = new ControllerContext() {HttpContext = new DefaultHttpContext(){User = mockUser}}
            };
        }

        [Test]
        public async Task RegisterUser_Ok()
        {
            Assert.IsTrue(Validator.TryValidateObject(user, new ValidationContext(user), new List<ValidationResult>(), true));
            var result = await _userController.RegisterUser(user);
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public async Task RegisterUserShortName_BadRequest()
        {
            var u1 = new UserRegisterModel
            {
                UserName = "T"
            };
            Assert.IsFalse(Validator.TryValidateObject(u1, new ValidationContext(u1), new List<ValidationResult>(), true));
        }
        [Test]
        public async Task RegisterUserLongName_BadRequest()
        {
            var u1 = new UserRegisterModel
            {
                UserName = "TestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUser"
            };
            Assert.IsFalse(Validator.TryValidateObject(u1, new ValidationContext(u1), new List<ValidationResult>(), true));
        }
        [Test]
        public async Task RegisterUserWrongName_BadRequest()
        {
            var u1 = new UserRegisterModel
            {
                UserName = "T%stßser"
            };
            Assert.IsFalse(Validator.TryValidateObject(u1, new ValidationContext(u1), new List<ValidationResult>(), true));
        }
        [Test]
        public async Task UserInfo_OkRequest()
        {
            var result = await _userController.GetUserInfo();
            Assert.IsInstanceOf<OkObjectResult>(result);
        }
    }
}