FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app 

# Copy csproj and restore as distinct layers
COPY scrum-cards-backend/scrum-cards-backend.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out .
# ENTRYPOINT ["dotnet", "scrum-cards-backend.dll"]

# Change the name of the app here if needed (TestApp)
CMD ASPNETCORE_URLS=http://*:$PORT dotnet scrum-cards-backend.dll